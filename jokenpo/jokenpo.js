const pedra = document.getElementById('pedra');
const papel = document.getElementById('papel');
const tesoura = document.getElementById('tesoura');
const divResult = document.getElementById('result');
const score = document.getElementById('score')
const userSpan = document.getElementById('user')
const pcSpan = document.getElementById('pc')

let user = 0
let pc = 0


function jokenpo(){
    let result = document.createElement('img')
    result.style.width = '100px'
    let random = Math.floor(Math.random() * 3 + 1)
    if (random === 1){
        result.src = './img/pedra2.png'
    } else if (random === 2){
        result.src = './img/papel2.png'
    }else{
        result.src = './img/tesoura2.png'
    }
    divResult.appendChild(result)
    return random
}

function placar(){
    userSpan.innerText = user
    pcSpan.innerText = pc
}
placar()

pedra.addEventListener('click', function(){
    divResult.innerText = ''
    let result = jokenpo()
    if (result === 1){
        //empate
    } else if (result === 2){
        //derrota
        pc++
    } else{
        //vitoria
        user++
    }
    placar()
})

papel.addEventListener('click', function(){
    divResult.innerText = ''
    let result = jokenpo()
    if (result === 1){
        //vitoria
        user++
    } else if (result === 2){
        //empate
    } else{
        //derrota
        pc++
    }
    placar()
})

tesoura.addEventListener('click', function(){
    divResult.innerText = ''
    let result = jokenpo()
    if (result === 1){
        //derrota
        pc++
    } else if (result === 2){
        //vitoria
        user++
    } else{
        //empate
    }
    placar()
})



// placar
//soma de pontos do computador, jogador, empate
//retorna a pontuação da partida e a quantidade de rodadas
//consolida os pontos no set